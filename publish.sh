set -eu

# Preriquisites:

# python3 -m venv venv
# ./venv/bin/activate
# pip install build

# Remove old builds
rm dist -rf

# Build
python -m build

# Upload to PyPi (will prompt for username and password)
twine upload  dist/*
