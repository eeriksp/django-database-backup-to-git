from django.apps import AppConfig


class DBBackupGitConfig(AppConfig):
    name = 'dbbackup_git'